$('.partners__items').slick({
    infinite: true,
    autoplay: true,
    arrows: false,
    slidesToShow: 4,
    slidesToScroll: 3,
});

$('.ecosystem__item-slider').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    nextArrow: '<button type="button" class="slick-next"><img src="images/svg/nextArrow.svg"></button>',
    prevArrow: '<button type="button" class="slick-prev"><img src="images/svg/prevArrow.svg"></button>',
})